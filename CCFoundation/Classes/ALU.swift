//
//  ALU.swift
//  CCFoundation
//
//  Created by kevin on 2021/5/20.
//

import Foundation

public class ALU {
    
    public static func add(a: Int, b: Int) -> Int {
        return a + b;
    }
    
    public static func sub(a: Int, b: Int) -> Int {
        return a - b;
    }
    
    public static func mul(a: Int, b: Int) -> Int {
        return a * b;
    }
    
    public static func div(a: Int, b: Int) -> Int {
        return a / b;
    }
}
